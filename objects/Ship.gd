extends KinematicBody2D

export (String, "player_1", "player_2", "player_3", "player_4") var player_number: String = "player_1"

export var thrust_power: float
export var max_speed: float
export var brake_power: float
export var velocity_damp: float

export var turning_power: float
export var max_turning: float
export var turning_damp: float

export var bounce_damp: float

export var max_health: float
export var max_energy: float
export var energy_regen: float

onready var health: float = max_health
onready var energy: float = max_energy

var heading: float = 0
var turning: float = 0
var velocity: Vector2 = Vector2(0, 0)

var using_energy: bool = false

func _physics_process(delta):
	# Turning
	apply_turning(delta)
	rotation = stepify(heading, 2*PI/16)
	
	var tp = false
	if Input.is_action_pressed(player_number + "_up"):
		tp = apply_thrust(delta)
	$Graphics/ThrustParticles.emitting = tp
	
	# Brakes
	var bp = false
	if Input.is_action_pressed(player_number + "_down"):
		bp = apply_brake(delta)
	$Graphics/BrakeParticles.emitting = bp
	
	# Damping n' Clamping
	if !tp:
		velocity *= pow(velocity_damp, delta)
	velocity = velocity.clamped(max_speed)
	
	# Move and process collisions
	var coll = move_and_collide(velocity * delta)
	if coll:
		velocity = velocity.bounce(coll.normal) * bounce_damp
		if coll.collider.has_method("impact"):
			coll.collider.impact(coll.normal*-velocity.length())
	
	# Regenerate energy
	if !using_energy:
		energy = min(energy + energy_regen * delta, max_energy)
	using_energy = false

func _input(event):
	if event.is_action_pressed(player_number + "_primary"):
		$PrimaryAction.activate()
	if event.is_action_released(player_number + "_primary"):
		$PrimaryAction.deactivate()
	if event.is_action_pressed(player_number + "_secondary"):
		$SecondaryAction.activate()
	if event.is_action_released(player_number + "_secondary"):
		$SecondaryAction.deactivate()

func apply_turning(delta: float):
	var new_turn_force = false
	if Input.is_action_pressed(player_number + "_left"):
		turning -= turning_power * delta
		new_turn_force = true
	if Input.is_action_pressed(player_number + "_right"):
		turning += turning_power * delta
		new_turn_force = true
	if !new_turn_force:
		turning *= pow(turning_damp, delta)
	turning = clamp(turning, -max_turning, max_turning)
	heading += turning * delta

func apply_thrust(delta: float) -> bool:
	# Direction vector and relative velocity
	var direction = Vector2(0, -1).rotated(heading)
	velocity += direction * thrust_power * delta
	
	return true

func apply_brake(delta: float) -> bool:
	velocity *= pow(1/brake_power, delta)
	return true

func impact(force: Vector2):
	velocity += force

func set_energy(new_energy) -> bool:
	if new_energy < energy:
		using_energy = true
	energy = clamp(new_energy, 0, max_energy)
	return !(energy == 0) 
