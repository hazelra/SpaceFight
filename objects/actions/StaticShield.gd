extends Area2D

export var ship_path: NodePath
export var energy_usage: float
export var min_energy: float

onready var ship: Node = get_node(ship_path)

var active = false

func activate():
	if ship.energy > min_energy:
		active = true
		$Static.visible = true
		$Shape.disabled = false

func deactivate():
	active = false
	$Static.visible = false
	$Shape.disabled = true

func _physics_process(delta):
	if active:
		if !ship.set_energy(ship.energy - energy_usage * delta):
			deactivate()
